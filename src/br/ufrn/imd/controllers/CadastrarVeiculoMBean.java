package br.ufrn.imd.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.ufrn.imd.model.Veiculo;


@ManagedBean
@SessionScoped
public class CadastrarVeiculoMBean {

	private Veiculo veiculo;
	
	private List<Veiculo> listagem;
	
	public CadastrarVeiculoMBean() {
		//iniciarValores();
		veiculo = new Veiculo();
		listagem = new ArrayList<Veiculo>();
	}

	private void iniciarValores() {
		/*onibus = new Veiculo();
		onibus.setMotorista(new Motorista());
		onibus.setLinha(new Linha());
		onibus.setEmpresa(new Empresa());
		onibus.setCobrador(new Cobrador());*/
		veiculo = new Veiculo();
		veiculo.setAno(0);
		veiculo.setChassi(new String());
		veiculo.setModelo(new String());
		veiculo.setPlaca(new String());
	}
	
	public String entrarCadastro(){
		return "/form_veiculo.jsf";
	}
	
	public String listar(){
		return "/list_veiculo.jsf";
	}
	
	public String voltar(){
		return "/index.jsf";
	}

	public String cadastrar() {
		listagem.add(veiculo);
		iniciarValores();
		FacesMessage msg = new FacesMessage("Ve�culo cadastrado com sucesso!");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage("", msg);
		return "/form_veiculo.jsf";
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public List<Veiculo> getListagem() {
		return listagem;
	}

	public void setListagem(List<Veiculo> listagem) {
		this.listagem = listagem;
	}
}
