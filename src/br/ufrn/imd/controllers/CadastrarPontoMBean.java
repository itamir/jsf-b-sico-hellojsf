package br.ufrn.imd.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.ufrn.imd.model.Motorista;
import br.ufrn.imd.model.Ponto;


@ManagedBean
@SessionScoped
public class CadastrarPontoMBean {

	private Ponto ponto;
	
	private List<Ponto> pontos;
	
	public CadastrarPontoMBean() {
		ponto = new Ponto();
		pontos = new ArrayList<Ponto>();
	}
	
	public String entrarCadastro(){
		return "/form_ponto.jsf";
	}
	
	public String voltar(){
		return "/index.jsf";
	}
	
	public String listar(){
		return "/list_pontos.jsf";
	}

	public String cadastrar() {
		
		
		ponto.setMotorista(new Motorista());
		pontos.add(ponto);
		ponto = new Ponto();
		FacesMessage msg = new FacesMessage("Ponto cadastrado com sucesso!");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage("", msg);
		return "/form_ponto.jsf";
	}

	public Ponto getPonto() {
		return ponto;
	}

	public void setPonto(Ponto ponto) {
		this.ponto = ponto;
	}

	public List<Ponto> getPontos() {
		return pontos;
	}

	public void setPontos(List<Ponto> pontos) {
		this.pontos = pontos;
	}

}
